var React = require('react');
//var ExamStore = require('../stores/ExamStore')


var ExamOptions = React.createClass({

  render: function(){
    return(
      <li>{this.props.opcao.descricao}</li>
    );
  }
});

var ExamQuestions = React.createClass({
  getInitialState: function(){
      return {opcao:""};
  },
  render: function(){
    return(
      <ol>
      <ExamOptions opcao={this.state.opcao}/>
      </ol>
    );
  },
  componentDidMount: function(){
    this.setState({opcao:{id:"1",descricao:"essa eh a opcao um"}});
  }
});

module.exports=ExamQuestions;
