var React=require('react');
var ExamsMenu=require('./ExamsMenu');
var ExamQuestions= require('./ExamsQuestions');

var ExamsApp = React.createClass({
  render:function(){
    return (
      <div>
        <ExamsMenu />
          examsapp
        <ExamQuestions />
      </div>
    );
  }
});

module.exports=ExamsApp;
